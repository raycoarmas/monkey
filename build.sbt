name := """RestApi"""
organization := "com.monkey"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.6"

libraryDependencies += guice
//database
libraryDependencies += javaJpa
libraryDependencies += "org.hibernate" % "hibernate-core" % "5.4.3.Final"
libraryDependencies += "org.postgresql" % "postgresql" % "42.2.5"
libraryDependencies += "org.glassfish" % "javax.el" % "3.0.1-b11"


PlayKeys.externalizeResources := false