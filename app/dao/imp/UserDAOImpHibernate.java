package dao.imp;

import dao.DatabaseExecutionContext;
import dao.UserDAO;
import model.User;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class UserDAOImpHibernate extends GenericDAOImpHibernate<User> implements UserDAO {

    private final JPAApi jpaApi;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public UserDAOImpHibernate(JPAApi jpaApi, DatabaseExecutionContext executionContext) {
        super(jpaApi,executionContext);
        this.jpaApi = jpaApi;
        this.executionContext = executionContext;
    }
    private <Role> Role wrap(Function<EntityManager, Role> function) {
        return jpaApi.withTransaction(function);
    }

    @Override
    public CompletionStage<User> findActiveUserByNamePassword(String name, String password) {
        return supplyAsync(() -> wrap(entityManager -> findActiveUserByNamePassword(entityManager, name, password)), executionContext);
    }

    @Override
    public CompletionStage<User> findUserByName(String name) {
        return supplyAsync(() -> wrap(entityManager -> findUserByName(entityManager, name)), executionContext);
    }

    private User findActiveUserByNamePassword(EntityManager entityManager, String name, String password) {
        try {
            return (User) entityManager.createQuery("select u from User u where name = :name and password = :password and active = true")
                    .setParameter("name", name)
                    .setParameter("password", password)
                    .getSingleResult();
        }catch (NoResultException e){
            return null;
        }
    }

    private User findUserByName(EntityManager entityManager, String name) {
        try {
            return (User) entityManager.createQuery("select u from User u where name = :name")
                    .setParameter("name", name)
                    .getSingleResult();
        }catch (NoResultException e){
            return null;
        }
    }

}
