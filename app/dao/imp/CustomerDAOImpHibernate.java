package dao.imp;

import dao.CustomerDAO;
import dao.DatabaseExecutionContext;
import model.Customer;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.function.Function;

public class CustomerDAOImpHibernate extends GenericDAOImpHibernate<Customer> implements CustomerDAO {

    private final JPAApi jpaApi;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public CustomerDAOImpHibernate(JPAApi jpaApi, DatabaseExecutionContext executionContext) {
        super(jpaApi,executionContext);
        this.jpaApi = jpaApi;
        this.executionContext = executionContext;
    }
    private <Role> Role wrap(Function<EntityManager, Role> function) {
        return jpaApi.withTransaction(function);
    }

}
