package dao.imp;

import dao.DatabaseExecutionContext;
import dao.GenericDAO;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class GenericDAOImpHibernate<T> implements GenericDAO<T> {

    private final JPAApi jpaApi;
    private final DatabaseExecutionContext executionContext;

    @Inject
    GenericDAOImpHibernate(JPAApi jpaApi, DatabaseExecutionContext executionContext) {
        this.jpaApi = jpaApi;
        this.executionContext = executionContext;
    }

    @Override
    public T insert(T t) throws ExecutionException, InterruptedException {
        return supplyAsync(() -> wrap(em -> insert(em, t)), executionContext).get();
    }

    @Override
    public CompletionStage<Stream> list(Class tclass) {
        return supplyAsync(() -> wrap(em -> list(em, tclass)), executionContext);
    }

    @Override
    public CompletionStage<T> findById(Long id, Class<T> tclass) {
        return supplyAsync(() -> wrap(em -> findById(em, id, tclass)), executionContext);
    }

    @Override
    public T merge(T t) throws ExecutionException, InterruptedException {
        return supplyAsync(() -> wrap(em -> merge(em, t)), executionContext).get();
    }

    @Override
    public void delete(T t) throws ExecutionException, InterruptedException {
        supplyAsync(() -> wrap(em -> delete(em, t)), executionContext).get();
    }

    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    private T insert(EntityManager em, T t) {
        if(t == null){
            return null;
        }
        em.persist(t);
        return t;
    }

    private Stream list(EntityManager em, Class tclass) {
        List list = em.createQuery("select t from "+ tclass.getCanonicalName() +" t").getResultList();
        return list.stream();
    }

    private T findById(EntityManager em, Long id, Class<T> tclass) {
        if(id == null) {
            return null;
        }
        return em.find(tclass, id);
    }

    private T merge(EntityManager em, T t) {
        if(t == null){
            return null;
        }
        em.merge(t);
        return t;
    }

    private Boolean delete(EntityManager em, T t) {
        if(t == null){
            return null;
        }
        em.remove(em.contains(t) ? t : em.merge(t));
        return true;
    }
}
