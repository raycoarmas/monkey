package dao;

import dao.imp.UserDAOImpHibernate;
import com.google.inject.ImplementedBy;
import model.User;

import java.util.concurrent.CompletionStage;


@ImplementedBy(UserDAOImpHibernate.class)
public interface UserDAO extends GenericDAO<User>{
    CompletionStage<User> findActiveUserByNamePassword(String name, String password);
    CompletionStage<User> findUserByName(String name);
}
