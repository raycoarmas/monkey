package dao;

import dao.imp.CustomerDAOImpHibernate;
import com.google.inject.ImplementedBy;
import model.Customer;

@ImplementedBy(CustomerDAOImpHibernate.class)
public interface CustomerDAO extends GenericDAO<Customer>{
}
