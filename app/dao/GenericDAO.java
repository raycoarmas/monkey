package dao;

import dao.imp.GenericDAOImpHibernate;
import com.google.inject.ImplementedBy;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

@ImplementedBy(GenericDAOImpHibernate.class)
public interface GenericDAO<T> {

    T insert(T t) throws ExecutionException, InterruptedException;
    CompletionStage<Stream> list(Class tclass);
    CompletionStage<T> findById(Long id, Class<T> tClass);
    T merge(T t) throws ExecutionException, InterruptedException;
    void delete(T t) throws ExecutionException, InterruptedException;
}
