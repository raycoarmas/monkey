package controllers;

import dao.UserDAO;
import dto.UserDTO;
import com.fasterxml.jackson.databind.JsonNode;
import helpers.GenericHelper;
import helpers.UserHelper;
import play.filters.csrf.AddCSRFToken;
import play.i18n.Messages;
import play.i18n.MessagesApi;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import security.JWTHelper;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;
import static play.libs.Json.toJson;

public class LoginController extends Controller {

    private final HttpExecutionContext ec;
    private final UserDAO userDAO;
    private final MessagesApi messagesApi;

    @Inject
    public LoginController(HttpExecutionContext ec, UserDAO userDAO, MessagesApi messagesApi) {
        this.ec = ec;
        this.userDAO = userDAO;
        this.messagesApi = messagesApi;
    }

    @AddCSRFToken
    public CompletionStage<Result> login(Http.Request request){

        Messages messages = messagesApi.preferred(request);
        JsonNode jsonNode = request.body().asJson();
        UserDTO userDTO = (UserDTO) GenericHelper.getObjectFromBody(jsonNode, UserDTO.class);
        if(userDTO == null){
            return supplyAsync(() -> notFound(messages.at("info.userEmpty")));
        }
        return userDAO.findActiveUserByNamePassword(userDTO.getName(), UserHelper.getSha512(userDTO.getPassword())).thenApplyAsync(user ->  {
            if(user == null || !user.getActive()){
                return unauthorized(messages.at("info.userNotFound"));
            }
            UserDTO resultDTO = UserHelper.userToUserDTO(user);
            resultDTO.setToken(JWTHelper.getJWT(resultDTO));
            return ok(toJson(resultDTO));
        },ec.current());

    }
}
