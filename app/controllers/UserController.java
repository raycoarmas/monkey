package controllers;

import business.UserBO;
import dao.UserDAO;
import dto.UserDTO;
import com.fasterxml.jackson.databind.JsonNode;
import helpers.GenericHelper;
import helpers.UserHelper;
import model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.i18n.Messages;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import security.Secured;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static java.util.concurrent.CompletableFuture.supplyAsync;
import static play.libs.Json.toJson;


public class UserController extends Controller {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final UserDAO userDAO;
    private final UserBO userBO;
    private final MessagesApi messagesApi;

    @Inject
    public UserController(UserDAO userDAO, UserBO userBO, MessagesApi messagesApi) {
        this.userDAO = userDAO;
        this.userBO = userBO;
        this.messagesApi = messagesApi;
    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> users(Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        return userDAO.list(User.class).thenCombine(userDAO.findById(loggedUser.getId(), User.class),(userStream, admin) -> {
            if (!UserHelper.isActiveAdminUser(admin)) {
                return unauthorized(messages.at("info.loggedAndAdmin"));
            }

            List<User> result =  (List<User>) userStream.collect(Collectors.toList());
            List<UserDTO> userDTOS = new ArrayList<>();
            result.forEach(user -> userDTOS.add(UserHelper.userToUserDTO(user)));
            return ok(toJson(userDTOS));
        });

    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> user(Long id, Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        return userDAO.findById(id, User.class).thenCombine(userDAO.findById(loggedUser.getId(), User.class),(user, admin) -> {
            if (!UserHelper.isActiveAdminUser(admin)) {
                return unauthorized(messages.at("info.loggedAndAdmin"));
            }

            if(user == null){
                return notFound(messages.at("info.userEmpty"));
            }
            return ok(toJson(UserHelper.userToUserDTO(user)));
        });
    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> createUser(Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        JsonNode jsonNode = request.body().asJson();
        UserDTO userDTO = (UserDTO) GenericHelper.getObjectFromBody(jsonNode, UserDTO.class);
        if (userDTO == null || userDTO.getName() == null || userDTO.getName().isEmpty()) {
            return supplyAsync(() -> badRequest(messages.at("info.userEmpty")));
        }

        return userDAO.findById(loggedUser.getId(), User.class).thenCombine(userDAO.findUserByName(userDTO.getName()), (admin, user) -> {
            if (!UserHelper.isActiveAdminUser(admin)) {
                return unauthorized(messages.at("info.loggedAndAdmin"));
            }

            try {
                if(user != null){
                    return status(Http.Status.CONFLICT, messages.at("info.userAlreadyExits", user.getName()));
                }
                return ok(toJson(userBO.createUser(userDTO)));

            } catch (Exception e) {
                log.error(e.getMessage());
                return internalServerError(messages.at("error.createUser"));
            }
        });
    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> updateUser(Http.Request request) {
        Messages messages = messagesApi.preferred(request);
        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        JsonNode jsonNode = request.body().asJson();
        UserDTO userDTO = (UserDTO) GenericHelper.getObjectFromBody(jsonNode, UserDTO.class);
        if (userDTO == null) {
            return supplyAsync(() -> notFound(messages.at("info.userEmpty")));
        }

        return userDAO.findById(loggedUser.getId(), User.class).thenCombine(userDAO.findById(userDTO.getId(), User.class), (admin, user) -> {
            if (!UserHelper.isActiveAdminUser(admin)) {
                return unauthorized(messages.at("info.loggedAndAdmin"));
            }
            if(user == null){
                return null;
            }
            try {
                if(userDTO.getName() != null && !userDTO.getName().isEmpty() && !user.getName().equals(userDTO.getName())){
                    User userWithSameName = userDAO.findUserByName(userDTO.getName()).toCompletableFuture().get();
                    if(userWithSameName != null){
                        return status(Http.Status.CONFLICT, messages.at("info.userAlreadyExits", userDTO.getName()));
                    }
                }

                UserDTO userUpdated = userBO.updateUser(user, userDTO);
                if(userUpdated == null){
                    return notFound(messages.at("info.userEmpty"));
                }
                return ok(toJson(userUpdated));
            } catch (Exception e) {
                log.error(e.getMessage());
                return internalServerError(messages.at("error.updateUser"));
            }

        });
    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> updateUserAdmin(Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        JsonNode jsonNode = request.body().asJson();
        UserDTO userDTO = (UserDTO) GenericHelper.getObjectFromBody(jsonNode, UserDTO.class);
        if (userDTO == null) {
            return supplyAsync(() -> notFound(messages.at("info.userEmpty")));
        }

        return userDAO.findById(loggedUser.getId(), User.class).thenApplyAsync(admin -> {
            if (!UserHelper.isActiveAdminUser(admin)) {
                return unauthorized(messages.at("info.loggedAndAdmin"));
            }

            try {
                UserDTO userUpdated = userBO.updateUserAdmin(userDTO);
                if(userUpdated == null){
                    return notFound(messages.at("info.userEmpty"));
                }
                return ok(toJson(userUpdated));
            } catch (Exception e) {
                log.error(e.getMessage());
                return internalServerError(messages.at("error.updateUser"));
            }
        });
    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> deleteUser(Long id, Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        return userDAO.findById(id, User.class).thenCombine(userDAO.findById(loggedUser.getId(), User.class),(user, admin) -> {
            if (!UserHelper.isActiveAdminUser(admin)) {
                return unauthorized(messages.at("info.loggedAndAdmin"));
            }

            try {
                userBO.deleteUser(user);
                return ok();
            } catch (Exception e) {
                log.error(e.getMessage());
                return internalServerError(messages.at("error.deleteUser"));
            }
        });
    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> activeUser(Long id, Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);
        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        return userDAO.findById(id, User.class).thenCombine(userDAO.findById(loggedUser.getId(), User.class),(user, admin) -> {
            if (!UserHelper.isActiveAdminUser(admin)) {
                return unauthorized(messages.at("info.loggedAndAdmin"));
            }

            try {
                return ok(Json.toJson(userBO.activeUser(user)));
            } catch (Exception e) {
                log.error(e.getMessage());
                return internalServerError(messages.at("error.deleteUser"));
            }
        });
    }
}
