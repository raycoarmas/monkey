package controllers;

import business.CustomerBO;
import dao.CustomerDAO;
import dao.UserDAO;
import dto.CustomerDTO;
import dto.UserDTO;
import helpers.CustomerHelper;
import helpers.UserHelper;
import model.Customer;
import model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.i18n.Messages;
import play.i18n.MessagesApi;
import play.libs.Files;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import security.Secured;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static java.util.concurrent.CompletableFuture.supplyAsync;
import static play.libs.Json.toJson;

public class CustomerController extends Controller {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final HttpExecutionContext ec;
    private final UserDAO userDAO;
    private final MessagesApi messagesApi;
    private final CustomerDAO customerDAO;
    private final CustomerBO customerBO;

    @Inject
    public CustomerController(HttpExecutionContext ec, UserDAO userDAO, MessagesApi messagesApi, CustomerDAO customerDAO, CustomerBO customerBO) {
        this.ec = ec;
        this.userDAO = userDAO;
        this.messagesApi = messagesApi;
        this.customerDAO = customerDAO;
        this.customerBO = customerBO;
    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> customers(Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        return customerDAO.list(Customer.class).thenCombine(userDAO.findById(loggedUser.getId(), User.class),(customerStream, user) -> {
            if(user == null || !user.getActive()){
                return unauthorized(messages.at("info.mustBeLogged"));
            }
            List<Customer> result =  (List<Customer>) customerStream.collect(Collectors.toList());
            List<CustomerDTO> customersDTO = new ArrayList<>();
            result.forEach(customer -> customersDTO.add(CustomerHelper.customerToCustomerDTO(customer)));
            return ok(toJson(customersDTO));
        });

    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> customer(Long id, Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        return userDAO.findById(loggedUser.getId(), User.class).thenCombine(customerDAO.findById(id, Customer.class), (user, customer) -> {
            if(user == null || !user.getActive()){
                return unauthorized(messages.at("info.mustBeLogged"));
            }
            if(customer == null){
                return notFound(messages.at("info.customerEmpty"));
            }
            return ok(toJson(CustomerHelper.customerToCustomerDTO(customer)));
        });
    }

    /*
mejora recibir el resto de parametros como uno solo en formato JSON
 */
    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> createCustomer(Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        Http.MultipartFormData<Files.TemporaryFile> body = request.body().asMultipartFormData();
        Http.MultipartFormData.FilePart<Files.TemporaryFile> photo = body.getFile("photo");
        Map userData = request.body().asMultipartFormData().asFormUrlEncoded();

        CustomerDTO customerDTO = CustomerHelper.mapToCustomerDTO(userData);
        if (customerDTO == null) {
            return supplyAsync(() -> notFound(messages.at("info.customerEmpty")));
        }
        if(customerDTO.getId() != null) {
            return supplyAsync(() -> badRequest(messages.at("error.IDCreation")));
        }

        return userDAO.findById(loggedUser.getId(), User.class).thenApplyAsync(user -> {
            if(user == null || !user.getActive()){
                return unauthorized(messages.at("info.mustBeLogged"));
            }
            try {
                return ok(toJson(customerBO.createCustomer(customerDTO, user, photo, messages, request.getHeaders().get("Host"))));
            } catch (Exception e) {
                log.error(e.getMessage());
                return internalServerError(messages.at("error.createCustomer"));
            }
        });

    }
/*
mejora recibir el resto de parametros como uno solo en formato JSON
 */
    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> updateCustomer(Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        Http.MultipartFormData<Files.TemporaryFile> body = request.body().asMultipartFormData();
        Http.MultipartFormData.FilePart<Files.TemporaryFile> photo = body.getFile("photo");
        Map userData = request.body().asMultipartFormData().asFormUrlEncoded();

        try {
            CustomerDTO customerDTO = CustomerHelper.mapToCustomerDTO(userData);
            if (customerDTO == null) {
                return supplyAsync(() -> notFound(messages.at("info.customerEmpty")));
            }

            return userDAO.findById(loggedUser.getId(), User.class).thenApplyAsync(user -> {
                if(user == null || !user.getActive()){
                    return unauthorized(messages.at("info.mustBeLogged"));
                }
                try {
                    CustomerDTO customerUpdated = customerBO.updateCustomer(customerDTO, user, photo, messages, request.getHeaders().get("Host"));
                    if(customerUpdated == null){
                        return notFound(messages.at("info.customerEmpty"));
                    }
                    return ok(toJson(customerUpdated));
                } catch (Exception e) {
                    log.error(e.getMessage());
                    return internalServerError(messages.at("error.updateCustomer"));
                }

            });
        }catch (NumberFormatException e){
            log.error(e.getMessage());
            return  supplyAsync(() -> badRequest(messages.at("error.idNumberError")));
        }
    }

    @Security.Authenticated(Secured.class)
    public CompletionStage<Result> deleteCustomer(Long id, Http.Request request) {
        Messages messages = messagesApi.preferred(request);

        UserDTO loggedUser = UserHelper.getUserDTOFromToken(request);

        if(loggedUser==null || loggedUser.getName() == null) {
            return supplyAsync(() -> unauthorized(messages.at("info.mustBeLogged")));
        }

        return customerDAO.findById(id, Customer.class).thenCombine(userDAO.findById(loggedUser.getId(), User.class),(customer, user) -> {
            if(user == null || !user.getActive()){
                return unauthorized(messages.at("info.mustBeLogged"));
            }
            if(customer == null){
                return notFound(messages.at("info.customerEmpty"));
            }
            try {
                customerBO.deleteCustomer(customer, messages);
                return ok();
            } catch (Exception e) {
                log.error(e.getMessage());
                return internalServerError(messages.at("error.deleteCustomer"));
            }
        });

    }
}
