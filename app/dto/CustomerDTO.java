package dto;

public class CustomerDTO {

    private Long id;
    private String name;
    private String surname;
    private String photoUri;
    private UserDTO creatorUser;
    private UserDTO lastModifier;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public UserDTO getCreatorUser() {
        return creatorUser;
    }

    public void setCreatorUser(UserDTO creatorUser) {
        this.creatorUser = creatorUser;
    }

    public UserDTO getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(UserDTO lastModifier) {
        this.lastModifier = lastModifier;
    }
}
