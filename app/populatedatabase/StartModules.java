package populatedatabase;

import com.google.inject.AbstractModule;

public class StartModules extends AbstractModule {

    @Override
    public void configure() {
        bind(PopulateDatabase.class).asEagerSingleton();
    }
}

