package populatedatabase;

import dao.UserDAO;
import helpers.UserHelper;
import model.User;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletionStage;

@Singleton
public class PopulateDatabase {

    @Inject
    public PopulateDatabase(UserDAO userDAO){
        CompletionStage<User> userCompletionStage =  userDAO.findActiveUserByNamePassword("admin",UserHelper.getSha512("123456"));
        userCompletionStage.thenAccept(user -> {
            if (user != null){
                return;
            }
            try{
                User userToInsert = new User();
                userToInsert.setName("admin");
                userToInsert.setPassword(UserHelper.getSha512("123456"));
                userToInsert.setAdmin(true);
                userToInsert.setActive(true);
                userDAO.insert(userToInsert);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}

