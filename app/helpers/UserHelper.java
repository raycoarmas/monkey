package helpers;

import dto.UserDTO;
import security.JWTHelper;
import model.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import play.mvc.Http;

import java.beans.FeatureDescriptor;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.stream.Stream;

public class UserHelper {

    public static UserDTO userToUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        userDTO.setPassword(null);

        return userDTO;
    }

    public static String getSha512(String value) {
        try {
            return new String(MessageDigest.getInstance("SHA-512").digest(value.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static UserDTO getUserDTOFromToken(Http.Request request) {
        String token = "";
        if (request.header("token").isPresent()) {
            token = request.header("token").get();
        }
        return JWTHelper.getJWTUserByToken(token);
    }


    public static String[] getNullPropertyNames(Object source) {
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
                .toArray(String[]::new);
    }

    public static boolean isActiveAdminUser(User admin) {
        return admin != null && admin.getAdmin() && admin.getActive();
    }
}
