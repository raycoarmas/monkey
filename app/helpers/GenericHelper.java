package helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class GenericHelper {

    private static final Logger LOG = LoggerFactory.getLogger(GenericHelper.class);

    public static Object getObjectFromBody(JsonNode jsonNode, Class t) {
        if(jsonNode == null){
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        Object object = null;
        try {
            object = mapper.readValue(jsonNode.toString(),t);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        return object;
    }

}
