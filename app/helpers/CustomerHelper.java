package helpers;

import dto.CustomerDTO;
import model.Customer;
import org.springframework.beans.BeanUtils;

import java.util.Map;

public class CustomerHelper {

    public static CustomerDTO customerToCustomerDTO(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO();
        BeanUtils.copyProperties(customer, customerDTO);
        customerDTO.setCreatorUser(UserHelper.userToUserDTO(customer.getCreatorUser()));
        if(customer.getLastModifier() != null){
            customerDTO.setLastModifier(UserHelper.userToUserDTO(customer.getLastModifier()));
        }
        return customerDTO;
    }


    public static CustomerDTO mapToCustomerDTO(Map userData) throws NumberFormatException {
        CustomerDTO customerDTO = new CustomerDTO();
        if(userData.get("id") != null) {
            customerDTO.setId(Long.valueOf(((String[]) userData.get("id"))[0]));
        }
        customerDTO.setName(((String[])userData.get("name"))[0]);
        customerDTO.setSurname(((String[])userData.get("surname"))[0]);
        return customerDTO.getName() == null || customerDTO.getName().isEmpty() ? null : customerDTO;
    }
}
