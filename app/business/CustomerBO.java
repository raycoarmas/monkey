package business;

import dao.CustomerDAO;
import dto.CustomerDTO;
import com.typesafe.config.Config;
import helpers.CustomerHelper;
import helpers.UserHelper;
import model.Customer;
import model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import play.Environment;
import play.i18n.Messages;
import play.libs.Files;
import play.mvc.Http;

import javax.inject.Inject;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;

public class CustomerBO {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final CustomerDAO customerDAO;
    private final Environment environment;
    private final Config config;

    @Inject
    public CustomerBO(CustomerDAO customerDAO, Environment environment, Config config) {
        this.customerDAO = customerDAO;
        this.environment = environment;
        this.config = config;
    }

    public CustomerDTO createCustomer(CustomerDTO customerDTO, User creatorUser, Http.MultipartFormData.FilePart<Files.TemporaryFile> photo, Messages messages, Optional<String> host) throws Exception {
        Customer newCustomer = new Customer();
        BeanUtils.copyProperties(customerDTO, newCustomer);
        newCustomer.setCreatorUser(creatorUser);
        customerDAO.insert(newCustomer);

        copyTempFile(photo, messages, newCustomer, host);
        try {
            customerDAO.merge(newCustomer);
        } catch (Exception e) {
            throw new CompletionException(e);
        }

        return CustomerHelper.customerToCustomerDTO(newCustomer);

    }


    public CustomerDTO updateCustomer(CustomerDTO customerDTO, User userModifier, Http.MultipartFormData.FilePart<Files.TemporaryFile> photo, Messages messages, Optional<String> host) throws ExecutionException, InterruptedException {
        return customerDAO.findById(customerDTO.getId(), Customer.class).thenApplyAsync(customer -> {
            if(customer == null){
                return null;
            }
            List<String> nullProperties = new ArrayList<>(Arrays.asList(UserHelper.getNullPropertyNames(customerDTO)));
            nullProperties.add("id");
            nullProperties.add("photoUri");
            nullProperties.add("creatorUser");
            BeanUtils.copyProperties(customerDTO, customer, nullProperties.toArray(new String[1]));
            customer.setLastModifier(userModifier);
            copyTempFile(photo, messages, customer, host);
            try {
                customerDAO.merge(customer);
            } catch (Exception e) {
                throw new CompletionException(e);
            }
            return CustomerHelper.customerToCustomerDTO(customer);
        }).toCompletableFuture().get();
    }

    private void copyTempFile(Http.MultipartFormData.FilePart<Files.TemporaryFile> photo, Messages messages, Customer customer, Optional<String> host) {
        if (photo != null) {
            Files.TemporaryFile file = photo.getRef();
            String stringFormat = String.format("%d", customer.getId());
            File folder = new File(environment.rootPath().toString() + config.getString("photos.storage") + customer.getId().toString());
            boolean createFolder = true;
            if (!folder.exists()) {
                createFolder = folder.mkdirs();
            }
            if (!createFolder) {
                log.error(messages.at("error.createFolder", folder));
                throw new CompletionException(new Throwable());
            }

            Path photoPath = Paths.get(folder + "/" + stringFormat + ".jpg");
            file.copyTo(photoPath, true);
            String urlPath = config.getString("photos.read.path") + customer.getId().toString() + "/" + stringFormat + ".jpg";
            if(host.isPresent()) {
                urlPath = host.get() + urlPath;
            }else{
                urlPath = config.getString("default.host.url")+ urlPath;
            }
            customer.setPhotoUri(urlPath);
        }
    }

    public void deleteCustomer(Customer customer, Messages messages) throws ExecutionException, InterruptedException {
        deleteObsoletePhotos(customer.getId(), messages);
        customerDAO.delete(customer);
    }

    private void deleteObsoletePhotos(Long id, Messages messages) {
        File folder = new File(environment.rootPath().toString() + config.getString("photos.storage") +id.toString());
        boolean createFolder = true;
        if (folder.exists()) {
            if(folder.list() != null) {
                Arrays.stream(folder.list()).forEach(file -> new File(folder + "/" + file).delete());
            }
            createFolder = folder.delete();
        }
        if (!createFolder) {
            log.error(messages.at("error.deleteFolder", folder));
            throw new CompletionException(new Throwable());
        }
    }
}
