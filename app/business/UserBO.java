package business;

import dao.UserDAO;
import dto.UserDTO;
import helpers.UserHelper;
import model.User;
import org.springframework.beans.BeanUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;


public class UserBO {

    private final UserDAO userDAO;

    @Inject
    public UserBO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public UserDTO createUser(UserDTO userDTO) throws ExecutionException, InterruptedException {
        User newUser = new User();
        BeanUtils.copyProperties(userDTO, newUser);
        newUser.setActive(true);
        newUser.setPassword(UserHelper.getSha512(newUser.getPassword()));
        userDAO.insert(newUser);
        return UserHelper.userToUserDTO(newUser);
    }

    public UserDTO updateUser(User user, UserDTO userDTO) {
        List<String> nullProperties = new ArrayList<>(Arrays.asList(UserHelper.getNullPropertyNames(userDTO)));
        nullProperties.add("id");
        BeanUtils.copyProperties(userDTO, user, nullProperties.toArray(new String[1]));
        try {
            userDAO.merge(user);
        } catch (Exception e) {
            throw new CompletionException(e);
        }
        user.setPassword(null);
        return UserHelper.userToUserDTO(user);
    }

    public UserDTO updateUserAdmin(UserDTO userDTO) throws ExecutionException, InterruptedException {
        return userDAO.findById(userDTO.getId(), User.class).thenApplyAsync(user -> {
            if(user == null){
                return null;
            }
            user.setAdmin(userDTO.getAdmin());
            try {
                userDAO.merge(user);
            } catch (Exception e) {
                throw new CompletionException(e);
            }
            return  UserHelper.userToUserDTO(user);
        }).toCompletableFuture().get();
    }

    public void deleteUser(User user) throws ExecutionException, InterruptedException {
        user.setActive(false);
        userDAO.merge(user);
    }

    public UserDTO activeUser(User user) throws ExecutionException, InterruptedException {
        user.setActive(true);
        userDAO.merge(user);
        return UserHelper.userToUserDTO(user);
    }
}
