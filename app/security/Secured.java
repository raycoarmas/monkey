package security;

import dto.UserDTO;
import play.i18n.Messages;
import play.i18n.MessagesApi;
import play.mvc.Http.Request;
import play.mvc.Result;
import play.mvc.Security;

import javax.inject.Inject;
import java.util.Optional;

public class Secured extends Security.Authenticator {


    private final MessagesApi messagesApi;

    @Inject
    public Secured(MessagesApi messagesApi) {
        this.messagesApi = messagesApi;
    }

    @Override
    public Optional<String> getUsername(Request request) {
        String token = "";
        if(request.header("token").isPresent()){
            token = request.header("token").get();
        }
        if(token.isEmpty()){
            return Optional.empty();
        }
        UserDTO loggedUser = JWTHelper.getJWTUserByToken(token);
        if(loggedUser!=null) {
            return Optional.ofNullable(loggedUser.getName());
        }
        return Optional.empty();
    }

    @Override
    public Result onUnauthorized(Request request) {
        Messages messages = messagesApi.preferred(request);
        return unauthorized(messages.at("info.userUnauthorized"));
    }
}
