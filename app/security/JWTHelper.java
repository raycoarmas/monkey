package security;

import dto.UserDTO;
import com.fasterxml.jackson.databind.JsonNode;
import helpers.GenericHelper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import play.libs.Json;

import javax.xml.bind.DatatypeConverter;
import java.util.HashMap;

public class JWTHelper {

    private static final String SECRET = "secret";

    public static String getJWT(UserDTO userDTO){
        HashMap<String, Object> claims = new HashMap<>() {{
            put("json", Json.stringify(Json.toJson(userDTO)));
        }};
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    public static UserDTO getJWTUserByToken(String jwt){
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET))
                    .parseClaimsJws(jwt).getBody();
            JsonNode userJson = Json.parse((String)claims.get("json"));
            return (UserDTO)GenericHelper.getObjectFromBody(userJson, UserDTO.class);
        } catch (Exception e) {
            return null;

        }

    }
}
